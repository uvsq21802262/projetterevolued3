package stockage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import utils.Helper;

import main.Solution;

public class HMap extends AbstractSolution implements ISolution {
	private HashMap<Solution, Solution> solutions;
	public HMap(){
		solutions = new HashMap<Solution, Solution>();
	}
	@Override
    public String toString(){
		return "HMap";
    }
	@Override
	public void afficher() {
		LinkedList<Solution> keys = new LinkedList<Solution>(solutions.keySet());
		System.out.println(" par bucket : ");
		for(Solution s : keys){
			solutions.get(s).afficher();
		}
		System.out.println("hsmap size " + solutions.size());
	}
	@Override
	public void ajouterSolution(Solution solution) {

		super.ajouterSolutionAff(solution);
		solution.table = Arrays.copyOf(solution.table, solution.table.length);
		Solution mapped = solutions.put(solution, solution);
		if(mapped == null) {
			incrSolutions();
		} else {
			System.out.println("existe deja solution");
		}
	}
	@Override
	public void reset() {
		solutions.clear();
		nbSolutions = 0;
		
	}
}
