package stockage;

import java.util.Comparator;

import utils.Helper;

import main.Solution;
//map : i -> map[i]   i donne l'indice à entrer et map[i] donne l'ordre de permutation
//supposons qu'on ait map = [3, 0, 1, 2]
//cela signifie que le bit le plus fort est le 3e bit dans la table de solution
//le bit le plus faible et le bit numero 2
public class ABRComparateurRandomise implements Comparator<Solution>{
	private int[] map;
	public ABRComparateurRandomise(int nbVar) {
		map = new int[nbVar];
		for (int i  = 0 ; i < nbVar ; i++) {
			map[i] = i;
		}
		shuffle();
		
	}
	public void shuffle() {
		Helper.shuffleArray(map);
		StringBuilder sb = new StringBuilder();
		sb.append("!!shuffle map : \n");
		for(int i = 0 ; i < map.length; i++)
			sb.append(map[i] + ", ");
		sb.append("\n");
		System.out.println(sb);
	}
	
	//pour comparer les solutions de plus de 32 bits..
	@Override
	public int compare(Solution o1, Solution o2) {
		int n = map.length;
		int i = 0;
		int j;
		while (i < n) {
			j = map[i];
			if (o1.table[j] && !o2.table[j])
				return 1;
			if (!o1.table[j] && o2.table[j])
				return -1;
			i++;
		}
		return 0;
	}

}
