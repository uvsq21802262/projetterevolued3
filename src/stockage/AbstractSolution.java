package stockage;

import main.Solution;

public abstract class AbstractSolution implements ISolution {
	protected int nbSolutions = 0;
	protected boolean bavard = false;
	public void setBavard(boolean b){
		bavard = b;
	}
	public void ajouterSolutionAff(Solution solution){
		if(bavard){
			System.out.print("ajout de : ");solution.afficher();}
	}
	public int nombreSolutions(){
		return nbSolutions;
	}
	
	protected void incrSolutions() {
//		System.out.println("incr");
		nbSolutions++;
	}
}
