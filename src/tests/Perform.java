package tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import main.Formule;
import main.Terme;
import main.Variable;
import performance.Simulateur;
import stockage.ABR;
import stockage.HMap;
import stockage.ISolution;
import stockage.Liste;
import stockage.ListeTriee;
import stockage.Trie;
import utils.DNFGenerator;

public class Perform {
	
	
	
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		int tailleMinTerme = 1;
		int nbMaxTermes = 3;
		double probaNot = 0.3;
		double probaVar = 0.5;
		int nbVar = 20;
		double debut;
		double fin;
		List<Terme.listingStrategy> strategy = new LinkedList<Terme.listingStrategy>();
		strategy.add(Terme.listingStrategy.TASK);
		strategy.add(Terme.listingStrategy.ITER);
		strategy.add(Terme.listingStrategy.REC);
		
		List<ISolution> stockage = new LinkedList<ISolution>();
		stockage.add(new ABR());
		stockage.add(new HMap());
		stockage.add(new Liste());
		stockage.add(new ListeTriee());
		stockage.add(new Trie(nbVar));
		boolean[] tab = {false, true};
		
		File fichier =  new File("Donnees.txt");
		PrintWriter writer = new PrintWriter(fichier, "UTF-8");
		for (ISolution s : stockage)
		{
			for(Terme.listingStrategy strat : strategy)
			{
				for(Boolean b : tab)
				{
					for(boolean extension: tab){
						writer.println("------------STOCKAGE "+s+", ");
						writer.println("STRATEGIE "+strat+", ");
						writer.println("alea : "+b+", ");
						writer.println("ext : "+extension+"-----------");
						Formule formule = DNFGenerator.generateDNF(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, b).setSolutionStrategy(s).setListingStrategy(strat);
						Simulateur simul = new Simulateur(formule,false);
						
						//debut = (System.currentTimeMillis()/1000.0);
						double[] resultat = simul.tester();
						//fin = (System.currentTimeMillis()/1000.0);
						writer.println("Dur�e: "+resultat[1]);
						writer.println("Nombre de solutions: "+resultat[0]);
						Variable.cpt = 0;
						s.reset();
						//writer.println("");
					}
				}
			}
		}
			
		writer.close();			
	}
}
