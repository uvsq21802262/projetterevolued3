package tests;

import main.Formule;
import main.Terme;
import stockage.ABR;
import stockage.ABREquilibre;
import stockage.ABRRandomise;
import stockage.ISolution;
import stockage.Trie;
import utils.DNFGenerator;
import utils.Helper;

public class TestABR {
	public static void main(String [] args) {
		int nbVar = 4;
		int nbMaxTermes = 4;
		int tailleMinTerme = 1;
		double probaNot = 0.5;
		double probaVar = 0.8;
		boolean aleatoire = true;
		double debut;
		double fin;
		ISolution abrRand = new ABRRandomise(nbVar);
		ISolution abr = new ABR();
		abrRand.setBavard(true);
		abr.setBavard(true);
		Formule f = DNFGenerator.generateDNF(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, aleatoire)
				.setSolutionStrategy(abrRand);
		Formule f2 = f.clone()
				.setSolutionStrategy(abr);
		
		System.out.println("ABR rand : ");
		System.out.println(f.afficher());
		debut = (System.currentTimeMillis()/1000.0);
		f.enleverDoublons();
		fin = (System.currentTimeMillis()/1000.0);

		f.trouverSolutions();
		f.solutions.afficher();
		System.out.println("nb sol : " +f.solutions.nombreSolutions());
		System.out.println("\nABR f2 : ");
		//System.out.println(f2.afficher());
		f2.enleverDoublons();

		//System.out.println(f2.afficher());
		f2.trouverSolutions();
		f2.solutions.afficher();
		System.out.println("nb sol : " +f.solutions.nombreSolutions());

	}
}
