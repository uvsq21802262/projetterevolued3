package tests;

import main.Formule;
import main.Terme;
import utils.DNFGenerator;

//test l'extension, le tri pour une meme formule
public class TestOrganisation {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int nbVar = 10000;
		int nbMaxTermes = 10;
		int tailleMinTerme = 1;
		double probaNot = 0.5;
		double probaVar = 0.8;
		boolean aleatoire = true;
		double debut;
		double fin;
		Formule f = DNFGenerator.generateDNF(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, aleatoire);
		Formule f2 = f.clone();
		

		System.out.println("tri f1 : ");
		//System.out.println(f.afficher());
		debut = (System.currentTimeMillis()/1000.0);
		f.enleverDoublons();
		fin = (System.currentTimeMillis()/1000.0);
		System.out.println(fin - debut);

		System.out.println("\ntri f2 : ");
		//System.out.println(f2.afficher());
		debut = (System.currentTimeMillis()/1000.0);
		f2.enleverDoublons();
		fin = (System.currentTimeMillis()/1000.0);
		System.out.println(fin - debut);

	}

}
