package performance;

import utils.Helper;
import main.Formule;

public class Simulateur{
	public double duree = 0.0;
	public int nbSolutions = 0;
	private boolean extension;
	private Formule formule;
	
	public Simulateur(Formule f, boolean extension) {
		//super();
		this.extension = extension;
		this.formule = f;
	}
	//resoud la solution et renvoit un tableau de double contenant 
	//1. le nombre de solutions
	//2. le temps de résolution
	public double[] tester() {
		double[] resultat = new double[2];
		Helper.afficherListeVar(formule.variables);
		System.out.println("Formule : "+formule.afficher());
		if(extension){
			formule.etendre();

			//System.out.println("Formule : "+formule.afficher());
		}
		formule.enleverDoublons();

		//System.out.println("Formule : "+formule.afficher());
		double debut = (System.currentTimeMillis()/1000.0);
		formule.trouverSolutions();
		double fin = (System.currentTimeMillis()/1000.0);
		System.out.println(fin - debut);
		System.out.println(formule.solutions.nombreSolutions()+ " solutions\nfin.");
		resultat[0] = formule.solutions.nombreSolutions();
		resultat[1] = fin - debut;
		return resultat;
	}
}
