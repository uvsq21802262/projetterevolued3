package utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import main.Formule;
import main.Terme;
import main.Variable;

public class DNFGenerator {
	//genere dans l'ordre
	//probanot probabilité qu'une variable soit negative
	//probava probabilité d'apparition d'une variable
	//

	
	public static Formule generateDNF(int nbVar, int nbTermes, int tailleMinTerme, 
			double probaNot, double probaVar, boolean aleatoire) {

		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, aleatoire);
		int i = 0;
		while(i++ < 9000 && formule.termes.size() != nbTermes) {
			Terme terme = new Terme();
			for (int j = 0 ; j < nbVar ; j++){
				Variable var = vf.getVariable();
				//variables.put(var, null);
				if (Math.random() < probaVar)
					terme.ajouterVariable(var, Math.random() < 1 - probaNot);
			}
			if(terme.taille() >= tailleMinTerme)
				formule.ajouterTerme(terme);

			vf.reset();
		}
		formule.variables = Arrays.asList(vf.variables);
		Collections.sort(formule.variables);
		formule.triee = !aleatoire;


		return formule;
		
	}

}

