package main;

public class Task {
    int nextPos;
    boolean val;
    Link variables;
    boolean invert;
    
    public Task(Link variables, boolean invert) {
        this.nextPos= -1;
        this.variables= variables;
        this.invert= invert;
    }
    public Task(int nextPos, boolean val, Link variables, boolean invert) {
        this.nextPos= nextPos;
        this.val= val;
        this.variables= variables;
        this.invert= invert;
    }
    public String toString() {
        String s;
        if (nextPos==-1)
            s= "";
        else
            s= String.format("nextPos=%d, val=%b, ", nextPos, val);
        s= s+String.format("variables = %s invert=%b", variables.toString(), invert);
        return s;   
    }
}
