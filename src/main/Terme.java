package main;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import stockage.ISolution;
import utils.Helper;


public class Terme {
	public TableauxVariables tab;

	public enum listingStrategy {
		ITER{ public String toString(){return "iter";} }, 
		REC{ public String toString(){return "rec";} }, 
		TASK{ public String toString(){return "task";} },
	};


	//public List<Variable> positifs;
	//public List<Variable> negatifs;
	
	
	public Terme() {
		//positifs = new ArrayList<Variable>();
		//negatifs = new ArrayList<Variable>();
		tab = new TableauxVariables();
	}

	public boolean contient(Variable v) {
		return tab.contient(v);
	}

	public void ajouterVariable(Variable v, boolean signe) {
		tab.ajouterVariable(v, signe);
	}

	public int taille() {
		return tab.taille();
	}

	public String toString() {
		return tab.toString();
	}

	public List<Terme> etendre(List<Variable> variables) {
		// il faut trier le terme avant et variables
		List<Terme> termes = new ArrayList<Terme>();

		if (variables.size() != 0) {
				Variable v = variables.remove(0);
				Terme t1 = this.clone();
				Terme t2 = this.clone();
				// id = la place de v dans le terme
				t1.tab.positifs.add(v.id, v);
				t1.tab.negatifs.add(v.id, null);
				t2.tab.positifs.add(v.id, null);
				t2.tab.negatifs.add(v.id, v);
				termes.addAll(t1.etendre(variables));
				termes.addAll(t2.etendre(variables));
				variables.add(0,v);
		} else {
			termes.add(this);
		}
		return termes;
	}
	//les var qui apparaissent uniquement
	public List<Variable> variables() {
		return tab.variables();
	}

	public Terme clone() {
		Terme t = new Terme();
		t.tab = this.tab.clone();
		return t;
	}

	public void trierTerme() {
		tab.trier();
	}

	public boolean equals(Terme t) {
		int n = t.taille();
		if (this.taille() != n)
			return false;
		for (int i = 0; i < n; i++) {
			if (t.tab.positifs.get(i) != this.tab.positifs.get(i)) {
				return false;
			}
		}
		return true;
	}

	public boolean estDansListe(List<Terme> termes) {
		for (Terme t : termes) {
			if (t.equals(this))
				return true;
		}
		return false;
	}

	//toutes les vars de la formule
	public boolean[] solutionFixe(int n) {
		boolean[] sol = new boolean[n];//tjrs initialisé a false false false
		for (int i = 0; i < this.taille(); i++) {
			Variable v = this.tab.positifs.get(i);
			if (v == null) {
				v = this.tab.negatifs.get(i);
				sol[v.id] = false;
			} else {
				sol[v.id] = true;
			}
		}
		return sol;
	}

//	//variables toutes les variables de la formule
	public void trouverSolutions(ISolution solutions, List<Variable> variables, listingStrategy strategy) {
        //System.out.println("Analyse du terme "+this);
		boolean[] table = solutionFixe(variables.size());
		List<Variable> varsUtilisees = this.variables();
		Link vars = null;
		for(Variable v : variables) 
			if(!varsUtilisees.contains(v)) 
				vars = new Link(v, vars);
		if(strategy == listingStrategy.TASK)
			solutionTask(solutions, table, vars);
		if (strategy == listingStrategy.REC)
			solutionRec(solutions, table, vars, false);
		if(strategy == listingStrategy.ITER)
			solutionIter(solutions, table, vars);
		
	}


	public void solutionTask(ISolution solutions, boolean[] table, Link vars) {
        Stack<Task> tasks= new Stack<Task>();
        tasks.push(new Task(vars, false));
        while (!tasks.empty()) {
            Task task= tasks.pop();
            vars= task.variables;
            boolean invert= task.invert;
            if (task.nextPos!= -1) {
                table[task.nextPos]= task.val;
            }
            if(vars != null) {
            	Variable v = vars.val;
            	vars = vars.next;
                int nextPos = v.id;
                if(invert){
                    Task t1= new Task(nextPos, true, vars, false);
                    Task t2= new Task(nextPos, false, vars, true);
                    tasks.push(t2);
                    tasks.push(t1);
                }else{
                    Task t1= new Task(nextPos, false, vars, false);
                    Task t2= new Task(nextPos, true, vars, true);
                    tasks.push(t2);
                    tasks.push(t1);
                }
            }else{
                //Helper.afficherSolution(table);
            	Solution s = new Solution((Arrays.copyOf(table, table.length))); 
                solutions.ajouterSolution(s);
                //s.afficher();
                
            }                   
        }   
    }
	
	public void solutionRec(ISolution solutions, boolean[] table, Link vars, boolean invert){
		if(vars != null) {
			int nextPos = vars.val.id;
			vars = vars.next;
			if(invert){
				table[nextPos] = true;
				solutionRec(solutions, table, vars, false);
				table[nextPos] = false;
				solutionRec(solutions, table, vars, true);
			}else{
				table[nextPos] = false;
				solutionRec(solutions, table, vars, false);
				table[nextPos] = true;
				solutionRec(solutions, table, vars, true);
			}
		}else{
			//Listes.afficherSolution(table);
			Solution s = new Solution((Arrays.copyOf(table, table.length)));
			solutions.ajouterSolution(s);
			//s.afficher();
		}
		
	}
	//mime l'arbre recursif
	private void solutionIter(ISolution solutions, boolean[] table, Link vars){
		if(vars == null){	//si var vide, solution deja complete
			solutions.ajouterSolution(new Solution(table));
			return;
		}
		boolean right = true;//va vers la droite 
		Link link = vars;
		Link last = vars.last;
		while (link != null || right) {//(i>=0)
			//si on arrive au bout on ajout la solution puis en revient a gauche
			//jusqu'à tomber sur un 0
			if (link == null) {
			//if (i == variables.size()){
				//Helper.afficherSolution(table);
				//System.out.println("ajout");
				solutions.ajouterSolution(new Solution(table));
				//ajouter solution ici
				//i--;
				link = last;
				right = false;
				continue;
			}
			//on remplace toutes les cases par 0 jusqu'a àa la derbiere
			Variable v = link.val;
			if (right) {
				table[v.id] = false;
				link = link.next;;
			}
			//sens gauche
			else {
				//si on tombe sur  un 0 on le remplace par un 1 et on revient à droite jusqu'au bout
				//et on remplacera toutes les val par 0
				if (!table[v.id]) {
					table[v.id] = true;
					right = true;
					link = link.next;
					continue;
				}
				link = link.before;
			}
		}
	}
}
