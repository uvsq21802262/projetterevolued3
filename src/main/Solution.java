package main;

import java.util.Arrays;

import utils.Helper;


public class Solution implements Comparable<Solution>{
	public boolean[] table;
	
	public Solution(boolean[] sol) {
		// table n'est qu'une REFERENce à l'initialisation il faudra a lajout (dans ISolution) faire une copie
		table = sol;
	}
	public void afficher() {
		String s = "";
		for(int i = 0; i<table.length;i++) {
			s+=((table[i])?1:0);
		}
		System.out.println(s + ((table.length > 32)?"":" : " + Helper.binaireAEntier(table)));
	}
	@Override
	public int hashCode(){
		int i = 0;
		int nbVar = table.length;
		//31 et pas 32 car un bit de signe
		int jump = 31;
		int cum = 0;
		int t;
		//n doit être inférieur ou égale à 32 et > 0 
		while(i + jump <  nbVar) {
			t = Helper.binaireAEntier(table, i, i + jump);
			cum = cum ^ t;
			i += jump;
		}
		if(i + jump >= nbVar ){
			t = Helper.binaireAEntier(table, i, table.length);
			cum = cum ^ t;
			
		}
		//System.out.println("Hash code : " + cum + " pour "); this.afficher();
		return cum;
		
	}
	@Override
	public int compareTo(Solution arg0) {
		//System.out.println("comparaison de "); this.afficher();
		//arg0.afficher();
		int i =  0;//le bit le plus fort est table[0]
		while (i != this.table.length) {
			if (this.table[i] && !arg0.table[i])
				return 1;
			if (!this.table[i] && arg0.table[i])
				return -1;
			i++;
		}
		return 0;
	}
	
	@Override
	public boolean  equals(Object other){
		if (! (other instanceof Solution))
			return false;
		return (this.compareTo(((Solution)other)) == 0);
	}
}
